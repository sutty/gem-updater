# gem-updater

This program works alongside Sutty's gem repository by looking into
available gems, finding newer versions on rubygems.org and preparing the
repository for building them through a CI.

Related projects:

* [gems](https://gitea.sutty.coop.ar/Sutty/gems.git): Contains a list of
  gems, versions and dependencies.

* [gem-compiler
  container](https://gitea.sutty.coop.ar/Sutty/containers-gem-compiler):
  Builds gems on a Woodpecker CI.

* [gem repository](https://gems.sutty.nl): Gem repository containing
  binary gems built and optimized for Alpine.

* [gem-compiler](https://github.com/luislavena/gem-compiler): A rubygems
  plugin for building gems.

## Installation

Install Crystal 1.3.2 and build tools.

```bash
make
```

## Usage

Run the program on a cronjob with the gem directory as argument:

```bash
git clone https://gitea.sutty.coop.ar/Sutty/gems.git
cd gems/
gem-updater . # a dot means "here, in the current directory"
git add build/
git commit -m gems
git push
```

Once you push and if the gem-compiler CI is configured, gems will be
built and pushed to gems.sutty.nl.

## Development

Just hack on (?)

## Contributing

1. Fork it (<https://0xacab.org/sutty/gem-updater>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
