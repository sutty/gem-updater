#!/bin/sh
set -e

# First argument is required
test -n "$1"

# It's a directory!
cd "$1"

# Bring changes and put local changes on top
git pull --rebase

# Update gems
gem-updater .

# Commit and push changes or do nothing
git add build/ &&
  git commit -m gem-updater &&
    git push || :
