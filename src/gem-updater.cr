require "log"
require "file_utils"
require "xml"
require "http/client"
require "./gem/version"

Log.setup_from_env

pwd = File.expand_path(ARGV[0])
client = HTTP::Client.new("rubygems.org", 443, true)
client.compress = true

# Move to gem builder directory
Dir.cd(pwd) do
  Log.debug { "Processing gems in #{pwd}" }
  # Find all buildable gems
  Dir.glob("build/*").tap do |glob|
    Log.debug { "Found #{glob.size} gems" }
  end.each do |dir|
    # Get gem name
    gem = File.basename(dir)
    # Try to find the latest version
    versions = [] of Gem::Version

    Log.info &.emit("Processing", gem: gem)

    Dir.glob("#{dir}/*") do |file|
      versions << Gem::Version.new(File.basename(file))
    # Ignore unparsable versions
    rescue ArgumentError
      Log.warn &.emit("Couldn't parse version", file: file)
    end

    # Find the latest version of each major version
    latest_versions = versions.reject(&.prerelease?).group_by(&.major).values.map(&.max)

    if latest_versions.empty?
      Log.warn &.emit("Gem doesn't have a usable version", gem: gem)
      next
    end

    latest_versions_files = latest_versions.map do |latest_version|
      [latest_version.major, File.join(dir, latest_version.to_s)]
    end.to_h

    latest_version = latest_versions.max

    Log.info &.emit("Latest versions", version: latest_versions.map(&.to_s).join(", "))

    # Get the version feed
    response = client.get "/gems/#{gem}/versions.atom"
    # Ignore errors
    unless response.success?
      Log.warn &.emit("Error while downloading versions feed", status: response.status_code)
      next
    end

    # Parse Atom
    atom = XML.parse(response.body.sub("<feed xmlns=\"http://www.w3.org/2005/Atom\">", "<feed>"))

    # Find versions newer than latest
    case (ids = atom.xpath("//entry/id"))
    when XML::NodeSet
      ids.map do |id|
        # Version is extracted from the ID URL
        id.content.split("/").last.strip
      # Only accept ruby platform
      end.reject do |id|
        id.includes?("-")
      # Find all unique versions newer than the local versions
      end.uniq.map do |version_string|
        Gem::Version.new(version_string)
      # Ignore unparsable versions
      rescue ArgumentError
        Log.warn &.emit("Couldn't parse", version: version_string)
        nil
      # Reject prereleases and versions that are not newer than the
      # major versions
      end.compact.reject(&.prerelease?).select do |version|
        comparable_version = latest_versions.find do |latest_version|
          version.major == latest_version.major
        end || latest_version

        version > comparable_version
      end.tap do |versions|
        Log.debug &.emit("Newer versions found", count: versions.size)

      # Create new versions from the latest file available.  If build
      # fails it will require manual intervention.
      end.each do |version|
        Log.info &.emit("New version", version: version.to_s)

        file = if latest_versions_files.has_key? version.major
                 latest_versions_files[version.major]
               else
                 latest_versions_files[latest_versions.max.major]
               end.to_s

        Log.info &.emit("Copying", file: file)

        FileUtils.cp(file, File.join(dir, version.to_s))
      end
    # XXX: #xpath can return several types.
    else
      Log.warn { "versions.atom wasn't on the expected format" }
    end

  # If there's no latest version, we skip the build entirely
  rescue Enumerable::EmptyError
    Log.warn { "Couldn't find latest version" }
  # Continue when feed is erroneous
  rescue XML::Error
    Log.warn { "Couldn't parse versions.atom" }
  end
end
