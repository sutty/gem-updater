# Port of Gem::Version to Crystal

module Gem
  class Version
    include Comparable(self)

    getter version : String

    VERSION_PATTERN = "[0-9]+(?>\.[0-9a-zA-Z]+)*(-[0-9A-Za-z-]+(\.[0-9A-Za-z-]+)*)?"
    ANCHORED_VERSION_PATTERN = /\A\s*(#{VERSION_PATTERN})?\s*\z/

    def initialize(version : String)
      version = "0" if version.is_a?(String) && version =~ /\A\s*\Z/

      @version = version.strip.gsub("-", ".pre.")

      raise ArgumentError.new("Malformed version number string #{version}") unless correct?
    end

    def <=>(other : self) : Int32
      return 0 if version == other.version

      lhsegments = canonical_segments
      rhsegments = other.canonical_segments

      return 0 if lhsegments == rhsegments

      lhsize = lhsegments.size
      rhsize = rhsegments.size
      limit  = (lhsize > rhsize ? lhsize : rhsize) - 1

      i = 0

      while i <= limit
        lhs =
          begin
            lhsegments[i]
          rescue IndexError
            0
          end
        rhs =
          begin
            rhsegments[i]
          rescue IndexError
            0
          end
        i += 1

        next      if lhs == rhs
        return  1 if lhs.is_a?(String) && rhs.is_a?(Int32)
        return -1 if lhs.is_a?(Int32) && rhs.is_a?(String)

        return lhs.to_i32 <=> rhs.to_i32
      end

      return 0
    end

    def major
      canonical_segments.first
    end

    def minor
      if canonical_segments.size > 1
        canonical_segments[1]
      else
        0
      end
    end

    def correct?
      !!(version.to_s =~ ANCHORED_VERSION_PATTERN)
    end

    def to_s
      version
    end

    def prerelease?
      !!(version =~ /[a-zA-Z]/)
    end

    def canonical_segments
      splitted_segments = split_segments

      splitted_segments.map do |segments|
        until segments.empty? || segments.last != 0
          segments.pop
        end

        segments
      end.flatten
    end

    def segments
      version.scan(/[0-9]+|[a-z]+/i).map do |s|
        /\A\d+\z/ =~ s[0] ? s[0].to_i32 : s[0]
      end
    end

    def split_segments
      parsed_segments = segments
      string_start = parsed_segments.index do |i|
        i.is_a? String
      end || parsed_segments.size

      [
        parsed_segments[0..(string_start - 1)],
        parsed_segments[string_start..-1]
      ]
    end
  end
end
