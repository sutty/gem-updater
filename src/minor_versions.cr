require "./gem/version"

versions = [] of Gem::Version

if ARGV.empty? || ARGV.includes?("-")
  STDIN.each_line do |file|
    versions << Gem::Version.new(File.basename(file))
  rescue e : ArgumentError
    STDERR.puts "not ok - #{e.message}"
  end
end

ARGV.each do |file|
  next if file == "-"

  versions << Gem::Version.new(File.basename(file))
rescue e : ArgumentError
  STDERR.puts "not ok - #{e.message}"
end

puts (versions.reject(&.prerelease?).group_by do |version|
  [version.major, version.minor].map(&.to_s).join(".")
end.values.map(&.max).map(&.to_s).join("\n"))
